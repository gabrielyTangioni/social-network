import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  hideMenus: boolean = false;

  constructor(private router: Router) { }

  ngOnInit() {
    this.verifySession()
  }

  verifySession() {
    if(localStorage.getItem('token')) {
      this.hideMenus = true;
    } else {
      this.hideMenus = false;
    }
    
  }

  logout() {
    localStorage.clear();
    window.location.reload();

  }

}
