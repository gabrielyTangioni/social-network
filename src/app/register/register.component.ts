import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Register } from '../models/register';
import { RegisterService } from '../services/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  myForm: FormGroup;
  register: any;
  alertSuccess: boolean;
  alertError: boolean;

  constructor(public formBuilder: FormBuilder, private service: RegisterService) {
    this.myForm = formBuilder.group({
      name: ["", Validators.required],
      nick: ["", Validators.required],
      password: ["", Validators.required],
    });

    this.register = new Register();
    this.alertSuccess = false;
    this.alertError = false;
   }

  ngOnInit() {
  }

  registers() {
    this.register.username = this.myForm.get('name').value;
    this.register.socialName = this.myForm.get('nick').value;
    this.register.password = this.myForm.get('password').value;

    this.service.register(this.register).subscribe(
      (response) => {
      this.alertSuccess = true;
      this.alertError = false;
      setTimeout(() => {
        window.location.reload();
      }, 3000);
    },
    (error) => {
      this.alertSuccess = false;
      this.alertError = true;
      setTimeout(() => {
        window.location.reload();
      }, 3000);
    })
  }

  

}
