import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Publication } from '../models/publication';
import { TimelineService } from '../services/timeline.service';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements OnInit {

  myForm: FormGroup;
  publication: any;
  timeline: any[] = [];
  config: any;
  page: number = 1;
  totalItems : any;

  constructor(public formBuilder: FormBuilder, private service: TimelineService) { 
    this.myForm = formBuilder.group({
      post: ["", Validators.required],
    });

    this.publication = new Publication();
  }

  ngOnInit() {
    this.getTimelineAll();
  }

  pageChanged(event) {
    console.log(event)
  }

  registerUpvote(id: any, status: any) {
    this.publication.postId = id;
    if(status == false) {
      this.service.registerUpvote(this.publication).subscribe(response => {
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      })
    } else {
      this.service.registerDownvote(id).subscribe(response => {
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      })
    }

  }

  getTimelineAll() {
    this.service.getTimelineAll().subscribe(response => {
      if(response && response.posts) {
        this.timeline = response.posts;
        this.totalItems = response.totalElements;
      }
    },
    error => {
    });

  }

  registerPublication() {
    this.publication.post = this.myForm.get('post').value;
    this.service.registerPublication(this.publication).subscribe(response => {
      alert("Publicação registrada com sucesso!");
      setTimeout(() => {
        window.location.reload();
      }, 500);
    })

  }

  pagination(page: any){
    this.service.getTimelineAllPagination(page-1).subscribe(response => {
      this.timeline = response.posts;
      this.totalItems = response.totalElements
    })
  }
}
