import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Register } from '../models/register';
import { Observable } from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class RegisterService {
    public url: string;

    constructor(public http: HttpClient) {
        this.url = environment.base_URL;
        
    }

    register(register : Register): Observable<any> {
        let params = JSON.stringify(register);
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(`${this.url}/user`, params,{headers: headers});
    }
}