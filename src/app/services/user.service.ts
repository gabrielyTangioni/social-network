import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user';

@Injectable()
export class UserService {
    public url: string;

    constructor(public http: HttpClient) {
        this.url = environment.base_URL;
    }

    login(user : User) {
        let params = JSON.stringify(user);
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post<any>(`${this.url}/authenticate`, params, {headers: headers})
    }
}