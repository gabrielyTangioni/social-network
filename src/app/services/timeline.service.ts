import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Publication } from '../models/publication';

@Injectable()
export class TimelineService {
    public url: string;

    constructor(public http: HttpClient) {
        this.url = environment.base_URL;
    }

    registerUpvote(id: Publication): Observable<any> {
        let params = JSON.stringify(id);
        return this.http.post(`${this.url}/upvote`, params);
    }

    registerDownvote(id: Publication): Observable<any> {
        console.log(id)
        return this.http.delete(`${this.url}/upvote?postId=${id}`);
    }

    // registerUpvoteDelete(): Observable<any> {
    //     let params = ''
    //     return this.http.delete(`${this.url}/post`, params);
    // }

    getTimelineAll(): Observable<any>  {
        return this.http.get(`${this.url}/post?page=0&size=10`);
    }

    getTimelineAllPagination(page: any): Observable<any>  {
        return this.http.get(`${this.url}/post?page=${page}&size=10`);
    }

    registerPublication(post: Publication): Observable<any>  {
        let params = JSON.stringify(post);
        return this.http.post(`${this.url}/post`, params);
    }

}