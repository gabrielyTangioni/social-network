import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';


@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {
    constructor(private service: UserService, private router: Router) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!request.url.endsWith("/user") && !request.url.endsWith("/authenticate")) {
            if(localStorage.getItem("token")) {
                request = request.clone({
                    setHeaders: {
                      Authorization: `Bearer ` + localStorage.getItem("token"),
                      "Content-Type": "application/json",
                    },
                });
            } else {
                localStorage.clear();
                this.router.navigate(['/login']);
            }
            
        }
        return next.handle(request).pipe(
            catchError((httpErrorResponse: HttpErrorResponse) => {
              const newHttpErrorResponse = new HttpErrorResponse( {
              });

              if(httpErrorResponse.status == 401) {
                  localStorage.clear();
                  this.router.navigate(['/login']);
               }
      
              return Observable.throw(newHttpErrorResponse);
            })
          );
    }
}