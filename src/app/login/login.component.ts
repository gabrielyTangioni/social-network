import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { User } from '../models/user';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  myForm: FormGroup;
  user: any;
  token: string;
  loading = false;
  alertError: boolean;

  constructor(private router: Router, public formBuilder: FormBuilder, private service: UserService) {
    this.myForm = formBuilder.group({
      username: ["", Validators.required],
      password: ["", Validators.required],
    });

    this.user = new User();
    this.alertError = false;
   }

  ngOnInit() {
  }

  timeline() {
    this.router.navigate(['/timeline']).then(() => {
      window.location.reload();
    });
  }

  login() {
    this.loading = true;
    this.alertError = false;
    this.user.username = this.myForm.get('username').value;
    this.user.password = this.myForm.get('password').value;

    this.service.login(this.user).subscribe((response) => {
      if(response && response.token) {
        localStorage.setItem("token", response.token);
        this.loading = false;        
        this.alertError = false;
        this.timeline();
      }
    },
    (error) =>  {
      this.loading = false;
      this.alertError = true;
      setTimeout(() => {
        window.location.reload();
      }, 3000);
  });
  }


}
